package com.rs2se.model;

/**
 * 
 * @author Fabrice L
 *
 */

public enum UpdateFlags {

	MAP_REGION,
	TELEPORTING,
	
	FORCED_MOVEMENT,
	GRAPHICS,
	ANIMATION,
	FORCED_CHAT,
	CHAT,
	APPEARANCE,
	FACE_ENTITY,
	FACE_DIRECTION,
	HIT,
	HIT_TWO
	
}
