package com.rs2se.model.player.updating;

import com.rs2se.model.player.Player;
import com.rs2se.network.packets.PacketBuilder;

/**
 * 
 * @author Fabrice L
 *
 */

public abstract class UpdatingBlock {

	public abstract void update(Player player, PacketBuilder buffer);
	
}