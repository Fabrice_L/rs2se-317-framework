package com.rs2se.model.player.updating.impl;

import com.rs2se.model.UpdateFlags;
import com.rs2se.model.player.Player;
import com.rs2se.model.player.updating.UpdatingBlock;
import com.rs2se.network.packets.PacketBuilder;

/**
 * 
 * @author Fabrice L
 *
 */

public class UpdatePlayerState extends UpdatingBlock {
	
	private boolean forced;
	private boolean chat;

	public UpdatePlayerState(boolean forced, boolean chat) {
		this.forced = forced;
		this.chat = chat;
	}

	@Override
	public void update(Player player, PacketBuilder buffer) {
		synchronized(player) {
			int mask = 0x0;
			if (player.getUpdateFlags().contains(UpdateFlags.FORCED_MOVEMENT)) {
				mask |= 0x400;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.GRAPHICS)) {
				mask |= 0x100;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.ANIMATION)) {
				mask |= 0x8;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.FORCED_CHAT)) {
				mask |= 0x4;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.CHAT) && !chat) {
				mask |= 0x80;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.APPEARANCE) || forced) {
				mask |= 0x10;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.FACE_ENTITY)) {
				mask |= 0x1;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.FACE_DIRECTION)) {
				mask |= 0x2;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.HIT)) {
				mask |= 0x20;
			}
			if (player.getUpdateFlags().contains(UpdateFlags.HIT_TWO)) {
				mask |= 0x200;
			}
			if (mask >= 0x100) {
				mask |= 0x40;
				buffer.put((byte) (mask & 0xFF));
				buffer.put((byte) (mask >> 8));
			} else {
				buffer.put((byte) mask);
			}
		}
	}

}
