package com.rs2se.model.player.updating.impl;

import com.rs2se.model.UpdateFlags;
import com.rs2se.model.player.Player;
import com.rs2se.model.player.updating.UpdatingBlock;
import com.rs2se.network.packets.PacketBuilder;

/**
 * 
 * @author Fabrice L
 *
 */

public class UpdateThisPlayerMovement extends UpdatingBlock {

	@Override
	public void update(Player player, PacketBuilder buffer) {
		if (player.getUpdateFlags().contains(UpdateFlags.TELEPORTING)
				|| player.getUpdateFlags().contains(UpdateFlags.MAP_REGION)) {
			buffer.putBits(1, 1);
			buffer.putBits(2, 3);
			buffer.putBits(2, player.getLocation().getHeight());
			buffer.putBits(1, 1);
			buffer.putBits(1, player.isUpdateRequired() ? 1 : 0);
			buffer.putBits(7, player.getLocation().localY());
			buffer.putBits(7, player.getLocation().localX());
		} else {
			if (player.getWalkingDirection() == -1) {
				if (player.isUpdateRequired()) {
					buffer.putBits(1, 1);
					buffer.putBits(2, 0);
				} else {
					buffer.putBits(1, 0);
				}
			} else {
				if (player.getRunningDirection() == -1) {
					buffer.putBits(1, 1);
					buffer.putBits(2, 1);
					buffer.putBits(3, player.getWalkingDirection());
					buffer.putBits(1, player.isUpdateRequired() ? 1 : 0);
				} else {
					buffer.putBits(1, 1);
					buffer.putBits(2, 2);
					buffer.putBits(3, player.getWalkingDirection());
					buffer.putBits(3, player.getRunningDirection());
					buffer.putBits(1, player.isUpdateRequired() ? 1 : 0);
				}
			}
		}
	}

}
