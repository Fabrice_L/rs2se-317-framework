package com.rs2se.model.player;

import java.util.LinkedList;
import java.util.List;

import org.jboss.netty.channel.Channel;

import com.rs2se.Settings;
import com.rs2se.model.Entity;
import com.rs2se.model.Location;
import com.rs2se.model.UpdateFlags;
import com.rs2se.network.packets.OutgoingPacket;
import com.rs2se.network.packets.outgoing.InitializePlayer;
import com.rs2se.network.packets.outgoing.SendMapRegion;
import com.rs2se.network.packets.outgoing.SendMessage;
import com.rs2se.utilities.ISAACCipher;

/**
 * 
 * @author Fabrice L
 *
 */

public class Player extends Entity {

	private Channel channel;
	
	private String username;
	private String password;
	
	private boolean isActive;
		
	private ISAACCipher inCipher;
	private ISAACCipher outCipher;
	
	public Player(String username, String password, Channel channel, ISAACCipher outCipher, ISAACCipher inCipher) {
		setUsername(username);
		setPassword(password);
		setChannel(channel);
		setOutCipher(outCipher);
		setInCipher(inCipher);
		setLocation(new Location(2606, 3093));
		getUpdateFlags().add(UpdateFlags.MAP_REGION);
	}
	
	public Player sendLogin() {
		setActive(true);
		send(new SendMapRegion());
		send(new SendMessage("Welcome to " + Settings.getServerName() + " [" + Settings.getProtocol() + "]"));
		
		send(new InitializePlayer());
		return this;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ISAACCipher getInCipher() {
		return inCipher;
	}

	public void setInCipher(ISAACCipher inCipher) {
		this.inCipher = inCipher;
	}

	public ISAACCipher getOutCipher() {
		return outCipher;
	}

	public void setOutCipher(ISAACCipher outCipher) {
		this.outCipher = outCipher;
	}
	
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
    public final void send(OutgoingPacket packet) {
    	channel.write(packet.send(this).toPacket());
    }
    
    public final List<Player> getLocalPlayers() {
    	return new LinkedList<Player>();
    }

}
