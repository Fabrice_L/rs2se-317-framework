package com.rs2se.model;

/**
 * 
 * @author Fabrice L
 *
 */

public class Location {

	private int x, y, height;

	public Location(int x, int y) {
		setX(x);
		setY(y);
		setHeight(0);
	}

	public Location(int x, int y, int z) {
		setX(x);
		setY(y);
		setHeight(z);
	}

	public Location setX(int x) {
		this.x = x;
		return this;
	}

	public int getX() {
		return x;
	}

	public Location setY(int y) {
		this.y = y;
		return this;
	}

	public int getY() {
		return y;
	}

	public Location setHeight(int height) {
		this.height = height;
		return this;
	}

	public int getHeight() {
		return height;
	}

	public int regionX() {
		return (x >> 3) - 6;
	}

	public int regionY() {
		return (y >> 3) - 6;
	}

	public int localX() {
		return localX(this);
	}

	public int localY() {
		return localY(this);
	}

	public int localX(Location location) {
		return x - 8 * location.regionX();
	}

	public int localY(Location location) {
		return y - 8 * location.regionY();
	}

	public Location transform(int x, int y) {
		return setX(getX() + x).setY(getY() + y);
	}

	public Location transform(int x, int y, int height) {
		return setX(getX() + x).setY(getY() + y).setHeight(getHeight() + height);
	}

	public Location copy() {
		return new Location(getX(), getY(), getHeight());
	}

	public boolean withinDistance(Location location) {
		if (height != location.getHeight()) {
			return false;
		}
		int diffX = Math.abs(location.getX() - x);
		int diffY = Math.abs(location.getY() - y);
		return diffX < 15 && diffY < 15;
	}
	
	public boolean withinDistance(Location location, int distance) {
		if (height != location.getHeight()) {
			return false;
		}
		int diffX = Math.abs(location.getX() - x);
		int diffY = Math.abs(location.getY() - y);
		return diffX < distance && diffY < distance;
	}

	public boolean sameAs(Location location) {
		return location.getX() == x && location.getY() == y && location.getHeight() == height;
	}

}