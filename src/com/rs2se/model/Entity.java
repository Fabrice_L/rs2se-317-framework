package com.rs2se.model;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author Fabrice L
 *
 */

public class Entity {
	
	private int slot;
	
	private Location location;
	
	private int walkingDirection = -1;
	private int runningDirection = -1;
	
	public int getSlot() {
		return slot;
	}
	
	public void setSlot(int slot) {
		this.slot = slot;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public final List<UpdateFlags> getUpdateFlags() {
		return new LinkedList<UpdateFlags>();
	}
	
    public final boolean isUpdateRequired() {
    	return !getUpdateFlags().isEmpty();
    }

	public int getWalkingDirection() {
		return walkingDirection;
	}

	public void setWalkingDirection(int walkingDirection) {
		this.walkingDirection = walkingDirection;
	}

	public int getRunningDirection() {
		return runningDirection;
	}

	public void setRunningDirection(int runningDirection) {
		this.runningDirection = runningDirection;
	}
    
}
