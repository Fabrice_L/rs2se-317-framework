package com.rs2se.network;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.DefaultChannelPipeline;

import com.rs2se.network.codec.PacketEncoder;
import com.rs2se.network.codec.LoginDecoder;

/**
 * 
 * @author Fabrice L
 *
 */

public class PipelineFactory implements ChannelPipelineFactory {

	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipe = new DefaultChannelPipeline();
		pipe.addLast("encoder", new PacketEncoder());
		pipe.addLast("decoder", new LoginDecoder());
		pipe.addLast("handler", new ChannelHandler());
		return pipe;
	}

}
