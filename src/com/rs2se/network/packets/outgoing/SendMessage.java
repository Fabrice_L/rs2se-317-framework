package com.rs2se.network.packets.outgoing;

import com.rs2se.model.player.Player;
import com.rs2se.network.packets.OutgoingPacket;
import com.rs2se.network.packets.IncomingPacket;
import com.rs2se.network.packets.PacketBuilder;

/**
 * 
 * @author Fabrice L
 *
 */

public final class SendMessage extends OutgoingPacket {

    private final String message;

    public SendMessage(String message) {
    	super(253);
    	this.message = message;
    }

	@Override
	public PacketBuilder send(Player player) {
		PacketBuilder packet = new PacketBuilder(getOpcode(), IncomingPacket.Type.VARIABLE);
		packet.putRS2String(message);
		return packet;
	}
}