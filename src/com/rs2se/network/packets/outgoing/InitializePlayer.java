package com.rs2se.network.packets.outgoing;

import com.rs2se.model.player.Player;
import com.rs2se.network.packets.IncomingPacket;
import com.rs2se.network.packets.OutgoingPacket;
import com.rs2se.network.packets.PacketBuilder;

/**
 * 
 * @author Fabrice L
 *
 */

public final class InitializePlayer extends OutgoingPacket {

	public InitializePlayer() {
		super(249);
	}

	@Override
	public PacketBuilder send(Player player) {
		PacketBuilder packet = new PacketBuilder(getOpcode(), IncomingPacket.Type.FIXED);
		packet.putByteA((byte)1);
		packet.putLEShortA(player.getSlot());
		return packet;
	}

}
