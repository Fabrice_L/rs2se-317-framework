package com.rs2se.network.packets.outgoing;

import com.rs2se.model.UpdateFlags;
import com.rs2se.model.player.Player;
import com.rs2se.model.player.updating.UpdatingBlock;
import com.rs2se.model.player.updating.impl.UpdatePlayerState;
import com.rs2se.model.player.updating.impl.UpdateThisPlayerMovement;
import com.rs2se.network.packets.IncomingPacket;
import com.rs2se.network.packets.OutgoingPacket;
import com.rs2se.network.packets.PacketBuilder;

/**
 * 
 * @author Fabrice L
 *
 */

public class PlayerUpdating extends OutgoingPacket {
	
	Player player;

	public PlayerUpdating() {
		super(81);
	}
	
	public final void append(UpdatingBlock block, PacketBuilder buffer, Player player) {
		block.update(player, buffer);
	}

	@Override
	public PacketBuilder send(Player player) {
		this.player = player;
		if (player.getUpdateFlags().contains(UpdateFlags.MAP_REGION)) {
		    player.send(new SendMapRegion());
		}
		PacketBuilder packet = new PacketBuilder(getOpcode(), IncomingPacket.Type.VARIABLE_SHORT);
		PacketBuilder block = new PacketBuilder(getOpcode(), IncomingPacket.Type.VARIABLE_SHORT);
		
		packet.startBitAccess();
		
		append(new UpdateThisPlayerMovement(), packet, player);
		
		if(player.isUpdateRequired()) {
			append(new UpdatePlayerState(false, true), block, player);
		}
		return packet;
	}

}
