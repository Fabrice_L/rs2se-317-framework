package com.rs2se.network.packets.outgoing;

import com.rs2se.model.player.Player;
import com.rs2se.network.packets.OutgoingPacket;
import com.rs2se.network.packets.PacketBuilder;

/**
 * 
 * @author Fabrice L
 *
 */

public final class SendMapRegion extends OutgoingPacket {

	public SendMapRegion() {
		super(73);
	}

	@Override
	public PacketBuilder send(Player player) {
		PacketBuilder packet = new PacketBuilder(getOpcode());
		packet.putShortA(player.getLocation().regionX() + 6);
		packet.putShort(player.getLocation().regionY() + 6);
		return packet;
	}

}
