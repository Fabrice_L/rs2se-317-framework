package com.rs2se.network.packets;

import com.rs2se.model.player.Player;

/**
 * 
 * @author Fabrice L
 *
 */

public abstract class OutgoingPacket {

    private final int opcode;

    public abstract PacketBuilder send(Player player);

    public OutgoingPacket(int opcode) {
	this.opcode = opcode;
    }

    public final int getOpcode() {
	return opcode;
    }
}