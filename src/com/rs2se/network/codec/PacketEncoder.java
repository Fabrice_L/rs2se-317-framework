package com.rs2se.network.codec;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

import com.rs2se.Main;
import com.rs2se.model.player.Player;
import com.rs2se.network.packets.IncomingPacket;
import com.rs2se.utilities.ISAACCipher;

/**
 * 
 * @author Fabrice L
 *
 */

public class PacketEncoder extends OneToOneEncoder {

	@Override
	protected Object encode(ChannelHandlerContext ctx, Channel channel, Object object) throws Exception {
		IncomingPacket packet = (IncomingPacket) object;

		if (packet.isRaw()) {
			return packet.getPayload();
		} else {
			ISAACCipher getCipher = null;
			for (Player player : Main.getPlayers()) {
				if (channel.equals(player.getChannel())) {
					getCipher = player.getOutCipher();
				}
			}
			ISAACCipher outCipher = getCipher;

			int opcode = packet.getOpcode();
			IncomingPacket.Type type = packet.getType();
			int length = packet.getLength();

			opcode += outCipher.getNextValue();

			int finalLength = length + 1;
			switch (type) {
			case VARIABLE:
				finalLength += 1;
				break;
			case VARIABLE_SHORT:
				finalLength += 2;
				break;
			case FIXED:
				break;
			}

			ChannelBuffer buffer = ChannelBuffers.buffer(finalLength);
			buffer.writeByte((byte) opcode);
			switch (type) {
			case VARIABLE:
				buffer.writeByte((byte) length);
				break;
			case VARIABLE_SHORT:
				buffer.writeShort((short) length);
				break;
			case FIXED:
				break;
			}

			buffer.writeBytes(packet.getPayload());

			return buffer;
		}
	}
	
}
