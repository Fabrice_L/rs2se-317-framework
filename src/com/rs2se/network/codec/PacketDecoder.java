package com.rs2se.network.codec;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.rs2se.Settings;
import com.rs2se.network.packets.IncomingPacket;
import com.rs2se.utilities.ISAACCipher;

/**
 * 
 * @author Fabrice L
 *
 */

public class PacketDecoder extends FrameDecoder {

	private int id = -1;
	
	private int length = -1;
	
	private ISAACCipher isaacCipher;
	
	public PacketDecoder(ISAACCipher isaacCipher) {
		this.isaacCipher = isaacCipher;
	}
	
	@Override
	protected Object decode(ChannelHandlerContext context, Channel channel, ChannelBuffer buffer) throws Exception {
		if (id == -1) {
			if (buffer.readableBytes() > 0) {
				id = (buffer.readUnsignedByte() - isaacCipher.getNextValue()) & 0xFF;
				length = Settings.getPacketSizes()[id];
			} else
				return null;
		}
		if (length == -1) {
			if (buffer.readableBytes() >= length) {
				byte[] bytes = new byte[length];
				buffer.readBytes(bytes);
				ChannelBuffer payLoad = ChannelBuffers.buffer(length);
				payLoad.writeBytes(bytes);
				IncomingPacket packet = new IncomingPacket(id, IncomingPacket.Type.FIXED, payLoad);
				id = length = -1;
				return packet;
			}
		}
		return null;
	}

}
