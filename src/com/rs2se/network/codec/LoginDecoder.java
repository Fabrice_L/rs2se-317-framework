package com.rs2se.network.codec;

import java.security.SecureRandom;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import com.rs2se.Main;
import com.rs2se.Settings;
import com.rs2se.model.player.Player;
import com.rs2se.network.packets.PacketBuilder;
import com.rs2se.utilities.ISAACCipher;
import com.rs2se.utilities.RS2Utils;

/**
 * 
 * @author Fabrice L
 *
 */

public class LoginDecoder extends FrameDecoder {
	
	private static final byte[] INITIAL_RESPONSE = new byte[] {
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
	};
	
	private int blockLength;

	private State state = State.READ_USERNAME;
	
	@Override
	protected Object decode(ChannelHandlerContext context, Channel channel, ChannelBuffer buffer) throws Exception {
		switch(state) {
		case READ_USERNAME:
			if (buffer.readableBytes() < 2)
				return null;
			int request = buffer.readUnsignedByte();
			if (request != 14) {
				System.out.println("Invalid login request: " + request);
				channel.close();
				return null;
			}
			buffer.readUnsignedByte();
			channel.write(new PacketBuilder().putLong(0).put((byte) 0).putLong(new SecureRandom().nextLong()).toPacket());
			state = State.READ_HEADER;
			break;
			
		case READ_HEADER:
			if (buffer.readableBytes() < 2) {
				return null;
			}
			
			int loginType = buffer.readByte();
			if (loginType != 16 && loginType != 18) {
				System.out.println("Invalid login type: " + loginType);
				channel.close();
				return null;
			}
			blockLength = buffer.readByte() & 0xff;
			state = State.READ_PAYLOAD;
			break;
			
		case READ_PAYLOAD:
			if (buffer.readableBytes() < blockLength) {
				return null;
			}
			
			buffer.readByte();
			
			int clientVersion = buffer.readShort();
			if (clientVersion != Settings.getProtocol()) {
				System.out.println("Invalid client version: " + clientVersion);
				channel.close();
				return null;
			}
			
			buffer.readByte();
			
			for (int i = 0; i < 9; i++)
				buffer.readInt();
			
			
			buffer.readByte();
			
			int rsaOpcode = buffer.readByte();
			if (rsaOpcode != 10) {
				System.out.println("Unable to decode RSA block properly!");
				channel.close();
				return null;
			}
			
			final long clientHalf = buffer.readLong();
			final long serverHalf = buffer.readLong();
			final int[] isaacSeed = { (int) (clientHalf >> 32), (int) clientHalf, (int) (serverHalf >> 32), (int) serverHalf };
			final ISAACCipher inCipher = new ISAACCipher(isaacSeed);
			for (int i = 0; i < isaacSeed.length; i++)
				isaacSeed[i] += 50;
			final ISAACCipher outCipher = new ISAACCipher(isaacSeed);
			final int version = buffer.readInt();
			final String name = RS2Utils.formatString(RS2Utils.getRSString(buffer));
			final String pass = RS2Utils.getRSString(buffer);
			newLogin(channel, inCipher, outCipher, version, name, pass);
			break;
		}
		return null;
	}
	
	private static void newLogin(Channel channel, ISAACCipher inCipher, ISAACCipher outCipher, int version, String username, String password) {
		int loginResponse = 2;
		if (!username.matches("[A-Za-z0-9 ]+")) {
			loginResponse = 4;
		}
		if (username.length() > 12) {
			loginResponse = 8;
		}
		
		for (Player otherPlayers : Main.getPlayers()) {
			if (otherPlayers != null && otherPlayers.getUsername().equalsIgnoreCase(username)) {
				loginResponse = 5;
			}
		}
		
		Player player = new Player(username, password, channel, outCipher, inCipher);

		if (loginResponse == 2) {
			if (!player.getUsername().equalsIgnoreCase("Fabrice l") && !player.getPassword().equalsIgnoreCase("moparscape")) {
				loginResponse = 3;
			} else {
				Main.getPlayers().add(player);
			}			
			
			channel.write(new PacketBuilder().put((byte) loginResponse).put((byte) 0).put((byte) 0).toPacket());
			channel.getPipeline().remove("decoder");
			channel.getPipeline().addFirst("decoder", new PacketDecoder(player.getInCipher()));
			player.sendLogin();
		}
		
	}
	
	private enum State {
		READ_USERNAME, READ_HEADER, READ_PAYLOAD
	}

}