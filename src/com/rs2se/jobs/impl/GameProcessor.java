package com.rs2se.jobs.impl;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.rs2se.Main;
import com.rs2se.model.player.Player;
import com.rs2se.network.packets.outgoing.PlayerUpdating;

/**
 * 
 * @author Fabrice L
 *
 */

public class GameProcessor implements Job {
	public void execute(JobExecutionContext context) throws JobExecutionException {

		for (Player player : Main.getPlayers()) {
			player.send(new PlayerUpdating());
		}
		
	}
 
}