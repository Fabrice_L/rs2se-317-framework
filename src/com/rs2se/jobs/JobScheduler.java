package com.rs2se.jobs;


import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Created by fabrice on 6-6-2015.
 */

public class JobScheduler {

    static Scheduler scheduler;

    public JobScheduler() throws SchedulerException{
        scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
    }

    public static void ScheduleRepeatForeverJob(int milliSeconds, Class<? extends Job> object) throws SchedulerException {

        JobDetail job = JobBuilder.newJob(object).build();
        
        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(
        		SimpleScheduleBuilder
        		.simpleSchedule()
        		.withIntervalInMilliseconds(milliSeconds)
        		.repeatForever()).build();
        
        scheduler.scheduleJob(job, trigger);
        
    }
}
