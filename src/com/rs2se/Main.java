package com.rs2se;


import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.quartz.SchedulerException;

import com.rs2se.jobs.JobScheduler;
import com.rs2se.jobs.impl.GameProcessor;
import com.rs2se.model.player.Player;
import com.rs2se.network.PipelineFactory;

/**
 * 
 * @author Fabrice L
 *
 */

public class Main {
	
	private static final ArrayList<Player> players = new ArrayList<Player>(Settings.getMaxPlayers());
	
	private static final Logger logger = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		logger.info("Starting up: " + Settings.getServerName() + " [Revision: " + Settings.getProtocol() +"]");
		logger.info("Binding to port: " + Settings.getPort());
		ServerBootstrap bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
		bootstrap.setPipelineFactory(new PipelineFactory());
		bootstrap.bind(new InetSocketAddress(Settings.getPort()));
		
		try {
			new JobScheduler();
			JobScheduler.ScheduleRepeatForeverJob(600, GameProcessor.class);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Player> getPlayers() {
		return players;
	}
	
	public static Player findPlayer(String name) {
		for (Player player : getPlayers())
			if (player.getUsername().equalsIgnoreCase(name))
				return player;
		return null;
	}
	
}
